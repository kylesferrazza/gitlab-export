package main

import (
	"log"
	"os"
	"os/exec"
	"path"
	"strings"
)

const cliReset = "\033[0m"
const cliRed = "\033[31m"
const cliGreen = "\033[32m"
const cliYellow = "\033[33m"

func main() {
	stat, err := os.Stat(outputDir)
	if os.IsNotExist(err) {
		log.Fatalln("Provided output path does not exist.")
	}
	if err != nil {
		log.Fatalln(err)
	}

	if !stat.IsDir() {
		log.Fatalln("Provided output path is not a directory.")
	}

	projects, err := doQuery()
	if err != nil {
		log.Fatalln(err)
	}

	for _, project := range projects {
		createOrUpdateRepo(project)
	}
}

func createOrUpdateRepo(info ProjectInfo) {
	curPath := path.Join(outputDir, info.RelPath)
	os.MkdirAll(curPath, 0755)
	output, err := exec.Command("git", "clone", "--mirror", info.SshUrl, curPath).CombinedOutput()
	if err != nil {
		if strings.Contains(string(output), "already exists") {
			output, err = updateRepo(info, curPath)
			if err != nil {
				log.Println("[", info.RelPath, "]:", cliRed, "Error updating existing clone:", strings.TrimSuffix(string(output), "\n"), cliReset)
				return
			}
		} else {
			log.Println("[", info.RelPath, "]:", cliRed, "Error cloning:", strings.TrimSuffix(string(output), "\n"), cliReset)
			return
		}
	} else {
		log.Println("[", info.RelPath, "]:", cliGreen, "Succesful clone.", cliReset)
	}
}

func updateRepo(info ProjectInfo, path string) ([]byte, error) {
	output, err := exec.Command("git", "--git-dir="+path, "remote", "update", "--prune").CombinedOutput()
	if err != nil {
		return output, err
	}
	strOutput := string(output)
	if strings.Contains(strOutput, "Fetching origin") {
		log.Println("[", info.RelPath, "]:", cliGreen, "Updated existing clone.", cliReset)
	} else {
		log.Println("[", info.RelPath, "]:", cliYellow, strings.TrimSuffix(string(output), "\n"), cliReset)
	}
	return []byte{}, nil
}
