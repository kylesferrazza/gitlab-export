package main

import (
	"flag"
	"log"
	"strings"
)

var gitlabHost string
var gitlabToken string
var outputDir string

func init() {
	flag.StringVar(&gitlabHost, "gitlab-host", "https://gitlab.com", "The address of the GitLab instance to export from")
	flag.StringVar(&gitlabToken, "gitlab-token", "", "Personal Access Token from GitLab")
	flag.StringVar(&outputDir, "output-dir", "", "Where to clone the backup repositories")
	flag.Parse()

	urlHasProtocol := strings.HasPrefix(gitlabHost, "http://") || strings.HasPrefix(gitlabHost, "https://")
	if !urlHasProtocol {
		log.Fatalln("GitLab URL must begin with 'http://' or 'https://'.")
	}

	if len(gitlabToken) == 0 {
		log.Fatalln("GitLab Token not supplied.")
	}

	if len(outputDir) == 0 {
		log.Fatalln("Output directory not supplied.")
	}
}
