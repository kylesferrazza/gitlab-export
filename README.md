# gitlab-export

Creates and updates full clones of all of the gitlab repositories for which the given access token's associated user is a member.

```
nix run gitlab:kylesferrazza/gitlab-export -- -gitlab-token $GITLAB_TOKEN -output-dir ~/gitlab/
```
