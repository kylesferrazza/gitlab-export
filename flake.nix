{
  description = "gitlab-export";

  outputs = { self, nixpkgs }:
  let
    pkgs = import nixpkgs {
      system = "x86_64-linux";
    };
  in {
    defaultPackage.x86_64-linux = pkgs.callPackage ./derivation.nix {};
    devShell.x86_64-linux = import ./shell.nix {
      inherit pkgs;
    };
  };
}
