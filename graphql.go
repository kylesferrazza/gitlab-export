package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

const gqlEndpoint = "/api/graphql"

var query = map[string]string{
	"query": `{
    projects(membership: true) {
      nodes {
        sshUrlToRepo
        fullPath
      }
    }
  }`,
}

type ProjectInfo struct {
	SshUrl  string
	RelPath string
}

type QueryResponse struct {
	Data struct {
		Projects struct {
			Nodes []struct {
				SshUrlToRepo string `json:"sshUrlToRepo"`
				FullPath     string `json:"fullPath"`
			} `json:"Nodes"`
		} `json:"projects"`
	} `json:"data"`
}

func doQuery() ([]ProjectInfo, error) {
	jsonQuery, err := json.Marshal(query)
	if err != nil {
		return nil, err
	}

	postUrl := gitlabHost + gqlEndpoint
	httpReq, err := http.NewRequest("POST", postUrl, bytes.NewBuffer(jsonQuery))
	if err != nil {
		return nil, err
	}

	httpReq.Header.Add("Authorization", "Bearer "+gitlabToken)
	httpReq.Header.Add("Content-Type", "application/json")
	res, err := http.DefaultClient.Do(httpReq)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	resBytes, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	queryRes := QueryResponse{}
	err = json.Unmarshal(resBytes, &queryRes)
	if err != nil {
		return nil, err
	}

	var projects []ProjectInfo
	for _, project := range queryRes.Data.Projects.Nodes {
		projects = append(projects, ProjectInfo{
			SshUrl:  project.SshUrlToRepo,
			RelPath: project.FullPath,
		})
	}

	return projects, nil
}
