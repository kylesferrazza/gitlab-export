{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  name = "gitlab-export";
  buildInputs = with pkgs; [
    go
    gopls
  ];
}
