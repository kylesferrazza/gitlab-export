{ buildGoModule }:
buildGoModule {
  name = "gitlab-export";
  version = "0.1";

  src = ./.;
  vendorSha256 = "pQpattmS9VmO3ZIQUFn66az8GSmB4IvYhTTCFn6SUmo=";
}
